import java.io.*;
import java.util.LinkedList;

public class BenutzerVerwaltungAdmin implements BenutzerVerwaltung {

    // default path
    String fileName = "./data.ser";
    LinkedList<Benutzer> userList;

    public BenutzerVerwaltungAdmin(){}

    public BenutzerVerwaltungAdmin(String fileName) {
        this.fileName = fileName;
    }

    /**
     * Init a file path for saving and retrieving data
     * @throws IOException If having troubles opening file
     */
    public void dbInit() throws IOException {
//        File fileIn = new File(fileName);
//
//        if (!fileIn.exists())
//            fileIn.createNewFile();

        userList = new LinkedList<>();
        writeUserList(userList);
    }

    /**
     * Read the saved user list from file
     * @return A {@link LinkedList} contains all saved {@link Benutzer}
     * @throws IOException If having trouble opening file
     * @throws ClassNotFoundException if having troubles with {@link Benutzer} class
     */
    @SuppressWarnings("unchecked")
    public LinkedList<Benutzer> getUserList() throws IOException, ClassNotFoundException {
        File fileIn = new File(fileName);

        FileInputStream fis = new FileInputStream(fileIn);
        ObjectInputStream ois = new ObjectInputStream(fis);


        try {
            userList = (LinkedList<Benutzer>) ois.readObject();

            fis.close();
            ois.close();

            return userList;
        } catch (ClassNotFoundException e) {
            fis.close();
            ois.close();
            throw new ClassNotFoundException();

        } catch (EOFException e) { // read an empty list results in a EOFException
            fis.close();
            ois.close();
            throw new EOFException();
        } finally {
            fis.close();
            ois.close();
        }
    }


    /**
     * Save the user list into a file
     * @param userList The user list to write
     * @throws IOException If having troubles opening file
     */
    private void writeUserList(LinkedList<Benutzer> userList) throws IOException {
        File fileOut = new File(fileName);

//        try {
            FileOutputStream fos = new FileOutputStream(fileOut);
            ObjectOutputStream oos = new ObjectOutputStream(fos);

            oos.writeObject(userList);

            oos.close();
            fos.close();
//        } finally {
//            fos.
//        }
    }

//    /**
//     * Delete all entries in user list
//     */
//    void dropTable() {
//        try {
//            writeUserList(new LinkedList<>());
//            dbInit();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }

    /**
     * Find a user in db with user id
     * @param userId the user id to check with
     * @return a {@link Benutzer} with the provided id, or null if not found
     */
    private Benutzer findUserWithId(String userId) throws IOException, ClassNotFoundException {
//        try {
            userList = getUserList();
            for (Benutzer user : userList) {
                if (userId.equals(user.userId)) {
                    return user;
                }
            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        return null;
    }

    /**
     * Check if this user is invalid
     * @param user the user to check
     * @return true if invalid, otherwise false
     */
    private boolean isInvalid(Benutzer user) {
        return ((user == null) ||
                (user.passWort.length == 0) ||
                (user.userId.length() == 0));
    }

    /**
     * Remove a {@link Benutzer} from the saved user list
     * @param benutzer The user to be removed
     * @throws Benutzer.UserDoesNotExistException if this user does not exist
     * @throws Benutzer.InvalidUserException if this user is invalid
     */
    public void benutzerLoeschen(Benutzer benutzer) throws Benutzer.UserDoesNotExistException,
        Benutzer.InvalidUserException, IOException, ClassNotFoundException {

        userList = getUserList();

        if (isInvalid(benutzer))
            throw new Benutzer.InvalidUserException();

        Benutzer target = findUserWithId(benutzer.userId);

        // if found no user with this ID, throw exception
        if (target == null)
            throw new Benutzer.UserDoesNotExistException();

        // else user found, just delete
//        try {


        userList.remove(target);

        writeUserList(userList);
//        } catch (IOException | ClassNotFoundException e) {
//            e.printStackTrace();
//        }
    }

    /**
     * Add a new {@link Benutzer} into the user list
     * @param benutzer The new {@link Benutzer} to add in.
     * @throws Benutzer.UserAlreadyExistsException if this user already exist in db
     * @throws Benutzer.InvalidUserException if this user is invalid
     * @throws Benutzer.PasswordTooShortException if this user's password is 3 chars long or shorter
     */
    @Override
    public void benutzerEintragen(Benutzer benutzer) throws Benutzer.UserAlreadyExistsException,
        Benutzer.InvalidUserException, Benutzer.PasswordTooShortException, ClassNotFoundException, IOException {

        if (isInvalid(benutzer))
            throw new Benutzer.InvalidUserException();

        // if this id already exists, throw an exception since 2 users cannot have the same ID
        if (benutzerVorhanden(benutzer))
            throw new Benutzer.UserAlreadyExistsException();

        if (benutzer.passWort.length <=3 )
            throw new Benutzer.PasswordTooShortException();

//        try {
            userList = getUserList();

            userList.addLast(benutzer);

            writeUserList(userList);
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

    /**
     * Check if a {@link Benutzer} with the same ID already exists in the user list
     * @param benutzer the {@link Benutzer} to check with
     * @return True if found, otherwise false
     * @throws Benutzer.InvalidUserException if this user is invalid
     */
    @Override
    public boolean benutzerVorhanden(Benutzer benutzer) throws Benutzer.InvalidUserException,
        IOException, ClassNotFoundException {
        if (isInvalid(benutzer))
            throw new Benutzer.InvalidUserException();

//        return (userList.contains(benutzer));


        Benutzer target = findUserWithId(benutzer.userId);

        return target != null;
    }
}

