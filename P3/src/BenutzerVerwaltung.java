import java.io.IOException;

public interface BenutzerVerwaltung {

    void benutzerEintragen(Benutzer benutzer) throws Benutzer.UserAlreadyExistsException,
        Benutzer.InvalidUserException, Benutzer.PasswordTooShortException, IOException, ClassNotFoundException;
    boolean benutzerVorhanden(Benutzer benutzer) throws Benutzer.InvalidUserException, IOException, ClassNotFoundException;
}

