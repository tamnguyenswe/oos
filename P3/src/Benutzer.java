import java.io.Serializable;

public class Benutzer implements Serializable {
    String userId;
    char[] passWort;

    Benutzer(String userId, char[] passWort) {
        this.userId = userId;
        this.passWort = passWort;
    }

    public static class UserAlreadyExistsException extends Exception {
        public UserAlreadyExistsException() {}

        public UserAlreadyExistsException(String errorMsg) {
            super(errorMsg);
        }
    }

    public static class UserDoesNotExistException extends Exception {
        public UserDoesNotExistException() {}

        public UserDoesNotExistException(String errorMsg) {
            super(errorMsg);
        }
    }

    public static class InvalidUserException extends Exception {
        public InvalidUserException() {}
    }

    public static class PasswordTooShortException extends Exception {
        public PasswordTooShortException() {}
    }

    @Override
    public boolean equals(Object other) {

        if (super.equals(other))
            return true;

        if (other instanceof Benutzer) {
            String thisPassword = String.copyValueOf(this.passWort);
            String otherPassword = String.copyValueOf(((Benutzer) other).passWort);

            return ((this.userId.equals(((Benutzer) other).userId)) &&
                thisPassword.equals(otherPassword));

        }

        return false;
    }
}

