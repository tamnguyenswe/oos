public class P2 {
    public static void main(String[] args) {
        BenutzerVerwaltungAdmin admin = new BenutzerVerwaltungAdmin();

        try {
            admin.benutzerEintragen(new Benutzer("foo", "password".toCharArray()));
            admin.benutzerEintragen(new Benutzer("bar", "p4ssword".toCharArray()));
            admin.benutzerEintragen(new Benutzer("anyideas", "p455word".toCharArray()));
            admin.benutzerEintragen(new Benutzer("someid", "p455vvord".toCharArray()));
            admin.benutzerEintragen(new Benutzer("outofnames", "p455vv0rd".toCharArray()));

            System.out.println("add a benutzer twice to trigger exception: ");
            admin.benutzerEintragen(new Benutzer("someid", "p455vvord".toCharArray()));
        } catch (Benutzer.UserAlreadyExistsException e) {
            System.out.println("User someid already exists!");
        } catch (Benutzer.InvalidUserException e) {
            System.out.println("Cannot save an invalid user");
        } catch (Benutzer.PasswordTooShortException e) {
            System.out.println("User password must be longer than 3 chars!");
        }

        System.out.println("trigger the invalid user exception: ");
        try {
            admin.benutzerEintragen(null);
        } catch (Benutzer.UserAlreadyExistsException e) {
            System.out.println("User already exists!");
        } catch (Benutzer.InvalidUserException e) {
            System.out.println("Cannot save an invalid user");
        } catch (Benutzer.PasswordTooShortException e) {
            System.out.println("User password must be longer than 3 chars!");
        }

        System.out.println("trigger short password exception");
        try {
            admin.benutzerEintragen(new Benutzer("shortpassword", "1".toCharArray()));
        } catch (Benutzer.UserAlreadyExistsException e) {
            System.out.println("User already exists!");
        } catch (Benutzer.InvalidUserException e) {
            System.out.println("Cannot save an invalid user");
        } catch (Benutzer.PasswordTooShortException e) {
            System.out.println("User password must be longer than 3 chars!");
        }

        System.out.println();

        System.out.println("check if user 0 exist in db");
        try {
            if (admin.benutzerVorhanden(new Benutzer("foo", "password".toCharArray())))
                System.out.println("User foo exists in db!");
            else
                System.out.println("User foo doesn't exist in db!");
        } catch (Benutzer.InvalidUserException e) {
            e.printStackTrace();
        }


        System.out.println("\nAll ids:");
        for (Benutzer benutzer : admin.getUserList())
            System.out.println(benutzer.userId);

        System.out.println("delete all entries");
        admin.dropTable();

        System.out.println("\nTable dropped");
        System.out.println("\nAll ids:\n");

        for (Benutzer benutzer : admin.getUserList()) {
            System.out.println(benutzer.userId);
        }

        System.out.println("check if user 0 exist in db:");
        try {
            if (admin.benutzerVorhanden(new Benutzer("foo", "password".toCharArray())))
                System.out.println("User foo exists in db!");
            else
                System.out.println("User foo doesn't exist in db!");
        } catch (Benutzer.InvalidUserException e) {
            System.out.println("Cannot search for an invalid user!");
        }


        try {
            admin.benutzerLoeschen(new Benutzer("foo", "password".toCharArray()));
        } catch (Benutzer.UserDoesNotExistException e) {
            System.out.println("User foo doesn't exist in db!");
        } catch (Benutzer.InvalidUserException e) {
            System.out.println("Cannot delete an invalid user");
        }

        System.out.println("trigger invalid user exception:");
        try {
            admin.benutzerLoeschen(null);
        } catch (Benutzer.UserDoesNotExistException e) {
            e.printStackTrace();
        } catch (Benutzer.InvalidUserException e) {
            System.out.println("Cannot delete an invalid user!");
        }

        System.out.println("trigger invalid user exception, again");
        try {
            admin.benutzerVorhanden(new Benutzer("", "".toCharArray()));
        } catch (Benutzer.InvalidUserException e) {
            System.out.println("Cannot search for an invalid user!");
        }
    }
}
