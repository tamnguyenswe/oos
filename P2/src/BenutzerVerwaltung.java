public interface BenutzerVerwaltung {

    void benutzerEintragen(Benutzer benutzer) throws Benutzer.UserAlreadyExistsException, Benutzer.InvalidUserException, Benutzer.PasswordTooShortException;
    boolean benutzerVorhanden(Benutzer benutzer) throws Benutzer.InvalidUserException;
}
