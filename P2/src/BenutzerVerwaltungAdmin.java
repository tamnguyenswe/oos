import java.util.LinkedList;

public class BenutzerVerwaltungAdmin implements BenutzerVerwaltung {

    private LinkedList<Benutzer> userList = new LinkedList<>();

    public LinkedList<Benutzer> getUserList() {
        return userList;
    }

    /**
     * Delete all entries in user list
     */
    void dropTable() {
        userList.clear();
    }

    /**
     * Find a user in db with user id
     * @param userId the user id to check with
     * @return a {@link Benutzer} with the provided id, or null if not found
     */
    private Benutzer findUserWithId(String userId) {
        for (Benutzer user : userList) {
            if (userId.equals(user.userId)) {
                return user;
            }
        }

        return null;
    }

    /**
     * Check if this user is invalid
     * @param user the user to check
     * @return true if invalid, otherwise false
     */
    private boolean isInvalid(Benutzer user) {
        return ((user == null) ||
                (user.passWort.length == 0) ||
                (user.userId.length() == 0));
    }

    /**
     * Remove a {@link Benutzer} from the saved user list
     * @param benutzer The user to be removed
     * @throws Benutzer.UserDoesNotExistException if this user does not exist
     * @throws Benutzer.InvalidUserException if this user is invalid
     */
    public void benutzerLoeschen(Benutzer benutzer) throws Benutzer.UserDoesNotExistException, Benutzer.InvalidUserException {

        if (isInvalid(benutzer))
            throw new Benutzer.InvalidUserException();

        Benutzer target = findUserWithId(benutzer.userId);

        // if found no user with this ID, throw exception
        if (target == null)
            throw new Benutzer.UserDoesNotExistException();

        // else user found, just delete
        userList.remove(target);
    }

    /**
     * Add a new {@link Benutzer} into the user list
     * @param benutzer The new {@link Benutzer} to add in.
     * @throws Benutzer.UserAlreadyExistsException if this user already exist in db
     * @throws Benutzer.InvalidUserException if this user is invalid
     * @throws Benutzer.PasswordTooShortException if this user's password is 3 chars long or shorter
     */
    @Override
    public void benutzerEintragen(Benutzer benutzer) throws Benutzer.UserAlreadyExistsException, Benutzer.InvalidUserException, Benutzer.PasswordTooShortException {

        if (isInvalid(benutzer))
            throw new Benutzer.InvalidUserException();

        // if this id already exists, throw an exception since 2 users cannot have the same ID
        if (benutzerVorhanden(benutzer))
            throw new Benutzer.UserAlreadyExistsException();

        if (benutzer.passWort.length <=3 )
            throw new Benutzer.PasswordTooShortException();

        userList.addLast(benutzer);
    }

    /**
     * Check if a {@link Benutzer} with the same ID already exists in the user list
     * @param benutzer the {@link Benutzer} to check with
     * @return True if found, otherwise false
     * @throws Benutzer.InvalidUserException if this user is invalid
     */
    @Override
    public boolean benutzerVorhanden(Benutzer benutzer) throws Benutzer.InvalidUserException {
        if (isInvalid(benutzer))
            throw new Benutzer.InvalidUserException();

        Benutzer target = findUserWithId(benutzer.userId);

        return target != null;
    }
}
