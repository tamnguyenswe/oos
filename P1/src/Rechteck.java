public class Rechteck extends Shape<Rechteck>{
    Point upperLeft;

    Point lowerRight;

    Rechteck() {}

    Rechteck(Point upperLeft, Point lowerRight) {
        this.upperLeft = upperLeft;
        this.lowerRight = lowerRight;
    }

    Rechteck(Rechteck reference) {
        this.upperLeft = reference.upperLeft;
        this.lowerRight = reference.lowerRight;
    }

    public Point getUpperLeft() {
        return upperLeft;
    }

    public void setUpperLeft(Point upperLeft) {
        this.upperLeft = upperLeft;
    }

    public Point getLowerRight() {
        return lowerRight;
    }

    public void setLowerRight(Point lowerRight) {
        this.lowerRight = lowerRight;
    }

    public double getArea() {
        if (area == -1) { // if never calculated area before
            int width = Math.abs(upperLeft.x - lowerRight.x);
            int height = Math.abs(upperLeft.y - lowerRight.y);

            area = width * height;
        }

        return area;
    }

    public boolean equals(Rechteck another) {
        return (this.lowerRight.equals(another.lowerRight) &&
                this.upperLeft.equals(another.upperLeft));
    }

    @Override
    public Rechteck clone() {
        return new Rechteck(this);
    }

    @Override
    public String toString() {
        return "Rechtangle upper left = " + upperLeft + ", lower right = " + lowerRight;
    }
}
