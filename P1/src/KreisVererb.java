public class KreisVererb extends Point {
    //  x & y = Centre's coordination

    double radius;

    /**
     * Default contructor
     */
    KreisVererb() {}


    /**
     * Init a new circle with its radius
     * @param radius New circle's radius
     */
    KreisVererb(double radius) {
        this.radius = radius;
    }


    /**
     * Init a new circle with its radius and centre point
     * @param centre New circle's centre
     * @param radius New circle's radius
     */
    KreisVererb(Point centre, double radius) {
        super(centre);

        this.radius = radius;
    }


    /**
     * Init a new circle with its radius and coordinations of its centre point
     * @param x Centre's x
     * @param y Centre's y
     * @param radius New circle's radius
     */
    KreisVererb(int x, int y, double radius) {
        super(x, y);
        this.radius = radius;
    }


    /**
     * Make a new copy of a circle
     * @param another The original circle
     */
    KreisVererb(KreisVererb another) {
        this.radius = another.radius;
        this.x = another.x;
        this.y = another.y;
    }


    double getRadius() {
        return radius;
    }


    public void setRadius(double radius) {
        this.radius = radius;
    }

    Point getCentre() {
        return new Point(this.x, this.y);
    }

    void setCentre(Point centre) {
        this.x = centre.x;
        this.y = centre.y;
    }


    /**
     * Always false, a centre cannot be compared to a point
     * @return Always false
     */
    @Override
    public boolean equals(Point p) {
        return false;
    }

    @Override
    public boolean equals(Object c) {
        if (super.equals(c))
            return true;

        if (c instanceof KreisVererb)
            return ((this.radius == ((KreisVererb) c).radius) &&
                    (this.x == ((KreisVererb) c).x) &&
                    (this.y == ((KreisVererb) c).y));

        return false;
    }

    @Override
    public String toString() {
        String centre = super.toString();
        return centre + "; radius = " + this.radius;
    }
}
